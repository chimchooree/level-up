# Prototype for a Leveling System
# To reach each new level, you must gain an additional 500 xp.

# A UI xp bar fills from 0 to the difference between current level + next level
# Find that next step (Level 1 = 1000 xp)
def find_step(level):
    # find step for next level, not current level
    level += 1 
    return level * 500

# Find the minimum total xp where you are the next level
# (Level 1 = 1500 xp)
def find_max_total(level):
    # find step for next level, not current level
    level += 1
    # Handle very low levels directly
    if level <= 0:
        return 0
    # Add a chunk of '500s' per level
    xp = 0
    i = 1
    while level >= 1:
        xp += 500 * i
        level -= 1
        i += 1
    return xp

# Use the number of '500s' to find level
def remainder_to_level(remainder):
    if remainder <= 0:
        return 0
    level = 1
    # take level out of 500s, where level starts at 1 + increases by 1 each round
    # your level is the last complete group of 'levels' you can remove 
    while level <= remainder:
        remainder = remainder - 1 * level
        if level <= remainder:
            level += 1
    return level

# Convert XP to Level
def find_level(xp):
    if xp <= 0:
        return 0
    if xp == 500:
        return 1
    remainder = 0
    # Take '500s' from xp until no longer able. 
    while xp > 500:
        xp -= 500
        remainder += 1
    # Use amount of '500s' in xp to determine level
    return remainder_to_level(remainder)

# Main Function
def main():
    current_xp = 4000
    current_level = find_level(current_xp)

    print("At " + str(current_xp) + " xp, you are Level " + str(find_level(current_xp)))

    print("At Level " + str(current_level - 1) + ", Step = " + str(find_step(current_level-1)) + ", Max xp = " + str(find_max_total(current_level-1)))
    print("At Level " + str(current_level) + ", Step = " + str(find_step(current_level)) + ", Max xp = " + str(find_max_total(current_level)))
    print("At Level " + str(current_level + 1) + ", Step = " + str(find_step(current_level + 1)) + ", Max xp = " + str(find_max_total(current_level + 1)))


## Run Program ##
main()
