# Leveling System

**Program Features:**

* Calculate level from given xp
* Calculate the difference between next level + current level (useful for UI xp bars)
* Calculate the total xp you will need to have gained to level up

## Requirements
* Python 2.7+

## Algorithm

**Leveling Algorithm:** To reach each new level, you must gain an additional 500 xp.

Ways to implement...

* make an xp table of arbitrary values to use, but the scope of leveling would be finite.
* programmatically fill a table to be the needed size with programmatic values, but it feels like a waste of memory to hold unimportant values
* employ a set of formulae to generate any needed values on the spot, but this would require manipulating + reversing the formulae to fit all the ways xp can be expressed. 

Route 3 is the most math-intensive, but I prefer its flexibility. 

Another thing worth noting is that all leveling information can be determined by a character's total xp. Storing his level is redundant.

## Prototype

I'm prototyping leveling for Dungeon Girls, a Godot Engine game. Prototyping for Godot Engine in Python has two main benefits. Python is very similar to Godot's GDScript, and Python is a fast, easy, enjoyable language to use in general.

As for the prototype, all I need is a functioning algorithm for testing the graphical + character level up systems. I already have a 'grant 500 xp' button, so using increments of 500 is easiest for me.

When I begin balancing the leveling system, thoughtfully incorporating square roots, limits, exponential growth, + other concepts would lead to a better design. Hopefully having made a simple version will make it easier to make a more complex one in the future.

### XP Table

For example, at 2000 total xp, you are between Level 2 (1500xp) + 3 (3000), so you are level 2.

| Level   | Step                      | Total         |
|---------|---------------------------|---------------|
| Level 0 | 0    xp                   | 0 total xp    |
| Level 1 | 500  xp between lvl 0 & 1 | 500 total xp  |
| Level 2 | 1000 xp between lvl 1 & 2 | 1500 total xp |
| Level 3 | 1500 xp between lvl 3 & 3 | 3000 total xp |

### For the algorithm, the number of '500s' is needed

For example, at 2000/500 = 4 500s, you are between Level 2 (3 500s) + 3 (6 500s), so you are level 2.

| Level   | Step                  | Total        |
|---------|-----------------------|--------------|
| Level 0 | 0 500s between levels | 0 500s total |
| Level 1 | 1 500  between levels | 1 500  total |
| Level 2 | 2 500s between levels | 3 500s total |
| Level 3 | 3 500s between levels | 6 500s total |
